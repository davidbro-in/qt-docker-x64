#Original: https://github.com/hadim/docker-qt
FROM ubuntu:18.04
MAINTAINER David Broin <davidmbroin@gmail.com>

# Install core dependencies

RUN apt-get update && \
    apt-get -y install -y \
        gcc g++ build-essential cmake bash libxcb1-dev libicu-dev \
        libssl-dev linux-headers-generic curl git libxrender-dev \
        libpng16-16 libpng-dev libjpeg-turbo8 libjpeg-turbo8-dev libicu60 libicu-dev \
        libgles2-mesa libgles2-mesa-dev libfreetype6-dev libsqlite3-dev \
	libgstreamer1.0-0 libgstreamer1.0-dev \
        libogg-dev libvorbis-dev bzip2 gperf bison ruby flex && \
    rm -rf /var/lib/apt/lists/*

# Choose Qt version

ENV QT_VERSION_MAJOR 5.14
ENV QT_VERSION 5.14.2

# Compile and install Qt Base

ENV QT_DIST /usr/local/Qt-5.14.2
ENV QT_BASE_SRC https://download.qt.io/archive/qt/5.14/5.14.2/submodules/qtbase-everywhere-src-5.14.2.tar.xz
ENV QT_BASE_DIR /qtbase-everywhere-src-5.14.2

RUN curl -sSL $QT_BASE_SRC | tar xJ \
    && cd $QT_BASE_DIR \
    && bash ./configure --help \
    && bash ./configure -opensource -confirm-license -static -no-accessibility -sql-sqlite -sqlite -no-qml-debug \
       -no-harfbuzz -openssl-linked -qt-pcre -no-dbus -nomake tools \
       -no-xkbcommon-evdev -no-xinput2 -no-xcb-xlib -no-glib -qt-xcb -no-compile-examples -nomake examples \
       -no-gif -qt-doubleconversion -no-gtk \
    && make install

ENV PATH $QT_DIST/bin:$PATH

# Compile and install Qt Script

ENV QT_SCRIPT_SRC https://download.qt.io/archive/qt/"$QT5_VERSION_MAJOR"/"$QT5_VERSION"/submodules/qtscript-everywhere-src-"$QT_VERSION".tar.xz
ENV QT_SCRIPT_DIR /everywhere-src-"$QT_VERSION"

RUN curl -sSL $QT_SCRIPT_SRC | tar xJ \
    && cd $QT_SCRIPT_DIR \
    && qmake \
    && make install \
    && cd /

# Compile and install Qt SVG

ENV QT_SVG_SRC https://download.qt.io/archive/qt/"$QT5_VERSION_MAJOR"/"$QT5_VERSION"/submodules/qtsvg-everywhere-src-"$QT_VERSION".tar.xz
ENV QT_SVG_DIR /qtsvg-everywhere-src-"$QT_VERSION"

RUN curl -sSL $QT_SVG_SRC | tar xJ \
    && cd $QT_SVG_DIR \
    && qmake \
    && make install \
    && cd /

# Compile and install Qt WebKit

ENV QT_WEBKIT_SRC https://download.qt.io/community_releases/"$QT_VERSION_MAJOR"/"$QT_VERSION"/qtwebkit-everywhere-src-"$QT_VERSION".tar.xz
ENV QT_WEBKIT_DIR /qtwebkit-everywhere-src-"$QT_VERSION"

#RUN curl -sSL $QT_WEBKIT_SRC | tar xJ \
#    && cd $QT_WEBKIT_DIR \
#    && qmake \
#    && make install \
#    && cd /

# Clean compilation files

RUN cd $QT_BASE_DIR && make clean \
    && cd $QT_SCRIPT_DIR && make clean \
    && cd $QT_SVG_DIR && make clean